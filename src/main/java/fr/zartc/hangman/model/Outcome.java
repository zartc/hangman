package fr.zartc.hangman.model;

public enum Outcome {
    WIN, LOSS
}

/* EOF */
