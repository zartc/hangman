package fr.zartc.hangman.service.promp;

import java.io.PrintStream;

import fr.zartc.hangman.model.LetterBag;
import fr.zartc.hangman.model.MaskedWord;
import fr.zartc.hangman.model.Outcome;


public class TersePrompt implements Prompt {
    protected final PrintStream out;

    public TersePrompt(PrintStream out) {
        this.out = out;
    }

    @Override
    public void display(int remainingAttemptCount, MaskedWord maskedWord, LetterBag letterBag) {
        out.printf("%d   %s   > ", remainingAttemptCount, stretchWord(maskedWord.getMask()));
    }

    @Override
    public void display(Outcome outcome, MaskedWord maskedWord) {
        switch (outcome) {
            case WIN:
                out.print("Bravo, you win.\n");
                break;
            case LOSS:
                out.printf("You loose, better luck next time! The word was %s\n", maskedWord.getWord());
                break;
        }
    }
}

/* EOF */
