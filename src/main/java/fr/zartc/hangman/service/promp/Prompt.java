package fr.zartc.hangman.service.promp;

import fr.zartc.hangman.model.LetterBag;
import fr.zartc.hangman.model.MaskedWord;
import fr.zartc.hangman.model.Outcome;


public interface Prompt {
    void display(int remainingAttemptCount, MaskedWord maskedWord, LetterBag letterBag);

    void display(Outcome outcome, MaskedWord maskedWord);

    default String stretchWord(String word) {
        var sb = new StringBuilder();

        for (var c : word.getBytes()) {
            sb.append((char)c).append(" ");
        }

        sb.deleteCharAt(sb.length() - 1); // remove the last " " char

        return sb.toString();
    }
}

/* EOF */
