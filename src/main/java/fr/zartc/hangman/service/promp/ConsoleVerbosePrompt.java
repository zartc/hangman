package fr.zartc.hangman.service.promp;

public class ConsoleVerbosePrompt extends VerbosePrompt {
    public ConsoleVerbosePrompt() {
        super(System.out);
    }
}

/* EOF */
