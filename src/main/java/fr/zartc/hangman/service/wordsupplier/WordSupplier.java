package fr.zartc.hangman.service.wordsupplier;

public interface WordSupplier {
    String nextWord();
}

/* EOF */
