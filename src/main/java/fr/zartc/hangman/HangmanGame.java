package fr.zartc.hangman;

import fr.zartc.hangman.model.LetterBag;
import fr.zartc.hangman.model.MaskedWord;
import fr.zartc.hangman.model.Outcome;
import fr.zartc.hangman.service.keyreader.KeyReader;
import fr.zartc.hangman.service.keyreader.KeyReaderImpl;
import fr.zartc.hangman.service.promp.ConsoleTersePrompt;
import fr.zartc.hangman.service.promp.Prompt;
import fr.zartc.hangman.service.wordsupplier.StaticListWordSupplier;


public class HangmanGame {
    private final Prompt prompt;
    private final KeyReader keyReader;

    public HangmanGame(KeyReader keyReader, Prompt prompt) {
        this.prompt = prompt;
        this.keyReader = keyReader;
    }

    public void run(String word, int maxAttempt) {
        var remainingAttemptCount = maxAttempt;
        var maskedWord = new MaskedWord(word);

        var letterBag = new LetterBag();

        while (remainingAttemptCount > 0) {
            prompt.display(remainingAttemptCount, maskedWord, letterBag);

            if (maskedWord.isDiscovered()) {
                prompt.display(Outcome.WIN, maskedWord);
                return;
            }

            var c = keyReader.readNext();

            if (letterBag.consume(c)) {
                if (maskedWord.matches(c) == 0) {
                    --remainingAttemptCount;
                }
            }
        }

        prompt.display(Outcome.LOSS, maskedWord);
    }

    public static void main(String[] args) throws Exception {
        // possible improvements:
        // check the args looking for a selector indicating which word list to use.
        // check the args looking for a selector (easy, normal, hard) to set the maxAttempt value.
        // run in a loop so that the game does not quit right after a single run.

        // possible evolutions:
        // replace the console KeyReader and prompt by a graphical IHM
        try (var keyReader = new KeyReaderImpl()) {
            var word = new StaticListWordSupplier().nextWord();
            var prompt = new ConsoleTersePrompt();

            new HangmanGame(keyReader, prompt).run(word, 10);
        }
    }
}

/* EOF */
