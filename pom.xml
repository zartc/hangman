<?xml version="1.0" encoding="UTF-8"?>
<!--suppress MavenModelInspection -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>fr.zartc</groupId>
    <artifactId>hangman</artifactId>
    <version>99-SNAPSHOT</version>

    <!-- Project Information -->
    <description>hangman "find the masked word" game</description>

    <properties>
        <java.version>17</java.version>
        <java.vendor>open</java.vendor>
        <maven.version>[3.8,)</maven.version>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <start-class>fr.zartc.hangman.HangmanGame</start-class>

        <skipTests>false</skipTests>            <!-- set to true to skip all tests -->
        <skipTests.ut>false</skipTests.ut>      <!-- set to true to skip only unit tests -->
        <skipTests.it>false</skipTests.it>      <!-- set to true to skip only integration tests -->

        <junit.version>5.9.0</junit.version>
        <assertj.version>3.23.1</assertj.version>
        <mockito.version>3.4.4</mockito.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.junit</groupId>
                <artifactId>junit-bom</artifactId>
                <version>${junit.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>org.assertj</groupId>
                <artifactId>assertj-core</artifactId>
                <version>${assertj.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <!-- Add a new source directory to our build -->
                    <execution>
                        <id>add-integration-test-sources</id>
                        <phase>generate-test-sources</phase>
                        <goals>
                            <goal>add-test-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/test-integration/java</source>
                            </sources>
                        </configuration>
                    </execution>
                    <!-- Add a new resource directory to our build -->
                    <execution>
                        <id>add-integration-test-resources</id>
                        <phase>generate-test-resources</phase>
                        <goals>
                            <goal>add-test-resource</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <directory>src/test-integration/resources</directory>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- unit tests -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skipTests>${skipTests.ut}</skipTests>
                </configuration>
            </plugin>

            <!-- integration tests -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <configuration>
                    <skipTests>${skipTests.it}</skipTests>
                </configuration>
                <executions>
                    <!-- Invokes both the integration-test and the verify goals of the Failsafe Maven plugin -->
                    <execution>
                        <id>integration-tests</id>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <release>${java.version}</release>
                    <useIncrementalCompilation>true</useIncrementalCompilation>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>flatten-maven-plugin</artifactId>
                <configuration>
                    <outputDirectory>${project.build.directory}</outputDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>flatten</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>flatten</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <!-- repackage the application and its dependencies as a Uber-jar.
                 see https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle/ -->
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>build-info</id>
                        <goals>
                            <goal>build-info</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>repackage</id>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                        <configuration>
                            <classifier>exec</classifier>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <!-- generate a properties file that can be use to assert the running version on production -->
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <verbose>false</verbose>
                    <offline>true</offline>
                    <failOnNoGitDirectory>false</failOnNoGitDirectory>
                    <dateFormat>yyyy-MM-dd'T'HH:mm:ssZ</dateFormat>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                    <generateGitPropertiesFilename>${project.build.outputDirectory}/META-INF/git.properties
                    </generateGitPropertiesFilename>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>create-app</id>
                        <phase>package</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <concat destfile="${project.basedir}/target/${project.artifactId}-app" binary="yes">
                                    <fileset file="${project.basedir}/src/assembly/stub.sh" />
                                    <fileset
                                        file="${project.basedir}/target/${project.build.finalName}-exec.${project.packaging}" />
                                </concat>
                                <chmod file="${project.basedir}/target/${project.artifactId}-app" perm="ugo+x" />
                            </target>
                        </configuration>
                    </execution>
                    <execution>
                        <id>echo-metadata</id>
                        <phase>none</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <!--suppress UnresolvedMavenProperty -->
                                <echo>used version ............. : ${jgitver.used_version}</echo>
                                <echo>version calculated ....... : ${jgitver.calculated_version}</echo>
                                <echo>branch_name .............. : ${jgitver.branch_name}</echo>
                                <echo>dirty .................... : ${jgitver.dirty}</echo>
                                <echo>detached head............. : ${jgitver.detached_head}</echo>
                                <echo>base commit on head ...... : ${jgitver.base_commit_on_head}</echo>
                                <echo>base_tag ................. : ${jgitver.base_tag}</echo>
                                <echo>head_tags ................ : ${jgitver.head_tags}</echo>
                                <echo>all_tags ................. : ${jgitver.all_tags}</echo>
                                <echo>all_version_tags ......... : ${jgitver.all_version_tags}</echo>
                                <echo>git_sha1_8 ............... : ${jgitver.git_sha1_8}</echo>
                                <echo>head_committer_name ...... : ${jgitver.head_committer_name}</echo>
                                <echo>head_committer_email ..... : ${jgitver.head_commiter_email}</echo>
                                <echo>head_commit_datetime ..... : ${jgitver.head_commit_datetime}</echo>
                            </target>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>validate</phase>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>${maven.version}</version>
                                </requireMavenVersion>
                                <requireJavaVersion>
                                    <version>${java.version}</version>
                                </requireJavaVersion>
                                <requireNoRepositories>
                                    <message>Best Practice is to not define repositories in pom.xml (use a repository
                                        manager instead)
                                    </message>
                                    <allowedRepositories>
                                        <allowedRepository>gitlab-maven</allowedRepository>
                                        <allowedRepository>spring-milestones</allowedRepository>
                                    </allowedRepositories>
                                    <allowedPluginRepositories>
                                        <allowedPluginRepository>spring-milestones</allowedPluginRepository>
                                    </allowedPluginRepositories>
                                </requireNoRepositories>
                                <requirePluginVersions>
                                    <message>Best Practice is to always define plugin versions!</message>
                                    <unCheckedPluginList>fr.brouillard.oss:jgitver-maven-plugin</unCheckedPluginList>
                                </requirePluginVersions>
                                <requireSameVersions>
                                    <plugins>
                                        <plugin>org.apache.maven.plugins:maven-surefire-plugin</plugin>
                                        <plugin>org.apache.maven.plugins:maven-failsafe-plugin</plugin>
                                        <plugin>org.apache.maven.plugins:maven-surefire-report-plugin</plugin>
                                    </plugins>
                                </requireSameVersions>
                                <banDistributionManagement>
                                    <allowSite>true</allowSite>
                                </banDistributionManagement>
                                <banDuplicatePomDependencyVersions />
                                <reactorModuleConvergence />
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>

        <!-- lock down plugins versions to avoid using Maven defaults (may be moved to a parent pom) -->
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-antrun-plugin</artifactId>
                    <version>3.0.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>3.3.0</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>3.3.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>3.1.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.9.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>3.2.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>3.0.0-M2</version>
                </plugin>
                <plugin>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>3.0.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>3.0.0-M5</version>
                </plugin>
                <plugin>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>3.0.0-M1</version>
                </plugin>
                <plugin>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>3.2.2</version>
                </plugin>
                <plugin>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>3.0.0-M5</version>
                </plugin>
                <plugin>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.2.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>3.10.0</version>
                </plugin>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.0.0-M5</version>
                </plugin>

                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>2.6.2</version>
                </plugin>
                <plugin>
                    <groupId>pl.project13.maven</groupId>
                    <artifactId>git-commit-id-plugin</artifactId>
                    <version>4.9.10</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>flatten-maven-plugin</artifactId>
                    <version>1.2.7</version>
                    <dependencies>
                        <dependency>
                            <groupId>fr.brouillard.oss</groupId>
                            <artifactId>jgitver-maven-plugin</artifactId>
                            <version>1.9.0</version>
                        </dependency>
                    </dependencies>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-project-info-reports-plugin</artifactId>
                    <version>3.1.2</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>3.3.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jxr-plugin</artifactId>
                    <version>3.1.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-report-plugin</artifactId>
                    <version>3.0.0-M5</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-pmd-plugin</artifactId>
                    <version>3.15.0</version>
                </plugin>
                <plugin>
                    <groupId>com.github.spotbugs</groupId>
                    <artifactId>spotbugs-maven-plugin</artifactId>
                    <version>4.5.3.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>3.1.2</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>taglist-maven-plugin</artifactId>
                    <version>2.4</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
            </plugin>
            <!-- order sensitive: must run before maven-jxr-plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <quiet>true</quiet>
                    <additionalOptions>
                        <additionalOptions>-package</additionalOptions>
                        <additionalOption>-Xdoclint:none</additionalOption>
                    </additionalOptions>
                </configuration>
            </plugin>
            <!-- order sensitive: must run after javadoc-plugin but before surefire-report, pmd, findbugs, checkstyle, taglist -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
            </plugin>
            <!-- @todo_ tag detection -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>taglist-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </reporting>

    <profiles>
        <profile>
            <id>toolchain</id>
            <activation>
                <property>
                    <name>!env.GITLAB_CI</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-toolchains-plugin</artifactId>
                        <version>3.0.0</version>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>toolchain</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <toolchains>
                                <!-- ensure the desired JDK version is used while building -->
                                <jdk>
                                    <version>${java.version}</version>
                                    <vendor>${java.vendor}</vendor>
                                </jdk>
                            </toolchains>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>display-jgitver-var</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-antrun-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>echo-metadata</id>
                                <phase>validate</phase>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                                <configuration>
                                    <target>
                                        <!--suppress UnresolvedMavenProperty -->
                                        <echo>used version ............. : ${jgitver.used_version}</echo>
                                        <echo>version calculated ....... : ${jgitver.calculated_version}</echo>
                                        <echo>branch_name .............. : ${jgitver.branch_name}</echo>
                                        <echo>dirty .................... : ${jgitver.dirty}</echo>
                                        <echo>detached head............. : ${jgitver.detached_head}</echo>
                                        <echo>base commit on head ...... : ${jgitver.base_commit_on_head}</echo>
                                        <echo>base_tag ................. : ${jgitver.base_tag}</echo>
                                        <echo>head_tags ................ : ${jgitver.head_tags}</echo>
                                        <echo>all_tags ................. : ${jgitver.all_tags}</echo>
                                        <echo>all_version_tags ......... : ${jgitver.all_version_tags}</echo>
                                        <echo>git_sha1_8 ............... : ${jgitver.git_sha1_8}</echo>
                                        <echo>head_committer_name ...... : ${jgitver.head_committer_name}</echo>
                                        <echo>head_committer_email ..... : ${jgitver.head_commiter_email}</echo>
                                        <echo>head_commit_datetime ..... : ${jgitver.head_commit_datetime}</echo>
                                    </target>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <!-- Ensure no x.x.x-SNAPSHOT dependencies are used when the CI build a release -->
            <id>pre-release-check</id>
            <activation>
                <property>
                    <name>env.GITLAB_CI</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-enforcer-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>release-check</id>
                                <phase>validate</phase>
                                <goals>
                                    <goal>enforce</goal>
                                </goals>
                                <configuration>
                                    <rules>
                                        <requireReleaseDeps />
                                    </rules>
                                    <fail>true</fail>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <!-- Ensure no x.x.x-SNAPSHOT dependencies are used when the CI build a release -->
            <id>release-check</id>
            <activation>
                <property>
                    <name>env.CI_COMMIT_TAG</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-enforcer-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>release-check</id>
                                <phase>validate</phase>
                                <goals>
                                    <goal>enforce</goal>
                                </goals>
                                <configuration>
                                    <rules>
                                        <requireReleaseDeps />
                                        <requireReleaseVersion />
                                    </rules>
                                    <fail>true</fail>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <!-- Enable the CI to deploy release artifacts -->
            <id>publish-maven-package</id>
            <activation>
                <property>
                    <name>env.GITLAB_CI</name>
                    <value>true</value>
                </property>
            </activation>
            <!-- where maven should deploy artifacts.
            Defined inside a profile so that only the CI is able to deploy artifacts -->
            <distributionManagement>
                <repository>
                    <id>gitlab-maven</id>
                    <!--suppress UnresolvedMavenProperty -->
                    <url>${env.CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}/packages/maven</url>
                </repository>
                <snapshotRepository>
                    <id>gitlab-maven</id>
                    <!--suppress UnresolvedMavenProperty -->
                    <url>${env.CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}/packages/maven</url>
                </snapshotRepository>
            </distributionManagement>
        </profile>
    </profiles>


    <url>https://gitlab.com/zartc/hangman.git</url>

    <scm>
        <url>${project.url}</url>
        <connection>scm:git:${project.scm.url}</connection>
    </scm>

    <developers>
        <developer>
            <name>Zart Colwing</name>
            <email>zartc@wanadoo.fr</email>
        </developer>
    </developers>

    <licenses>
        <license>
            <name>Private Source Code</name>
        </license>
    </licenses>

    <inceptionYear>2021</inceptionYear>

</project>
